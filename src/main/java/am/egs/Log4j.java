package am.egs;

/**
 * Created by haykh on 4/29/2019.
 */

import org.apache.log4j.Logger;

public class Log4j {
  private static Logger logger = Logger.getLogger(Log4j.class);
  public static void main(String[] args)
  {
    Log4j logObj = new Log4j();
    logObj.showLogLavel();
  }

  private void showLogLavel() {
    logger.trace("log4j trace");
    logger.debug("log4j debug");
    logger.info("log4j info");
    logger.warn("log4j warn");
    logger.error("log4j error");
    logger.fatal("log4j trace");
  }
}